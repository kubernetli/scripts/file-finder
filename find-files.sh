#!/bin/bash

## -- Default Settings
SOURCE_DIRECTORY="."
EXTENSIONS="yml,yaml"
EXCLUDES=".gitlab-ci.yml,node_modules,__pycache__"

## -- Help Context
show_help() {
cat << EOF
Usage: ${0##*/} [-h] [-s] "source_dir" [-e] "ext1,ext2" [-E] "execlude1,execlude2" [-c] "mycommand $file"
    -s source_dir      Cache-File [Default: '.']
    -e extensions      Comma separated list of extensions [Default: 'yml,yaml']
    -E excludes        Comma separated list of excluded files/directories [Default: '.gitlab-ci.yml,node_modules,__pycache__']
    -c command         Execute command with file placeholer. Use 'FILE' as placeholder [Example: 'yamllint FILE']
    -h                 Show this context
EOF
}

## -- Opting arguments
OPTIND=1; # Reset OPTIND, to clear getopts when used in a prior script
while getopts ":hs:e:E:c:" opt; do
  case ${opt} in
    s)
       SOURCE_DIRECTORY="${OPTARG}";
       ;;
    e)
       EXTENSIONS="${OPTARG}";
       ;;
    E)
       EXCLUDES="${OPTARG}";
       ;;
    c)
       COMMAND="${OPTARG}";
       ;;
    h)
       show_help
       exit 0
       ;;
    ?)
       echo "Invalid Option: -$OPTARG" 1>&2
       exit 1
       ;;
  esac
done
shift $((OPTIND -1))

## -- Check if Directory exists
if ! [[ -d "${SOURCE_DIRECTORY}" ]]; then echo "Directory ${SOURCE_DIRECTORY} does not exist!" && exit 1; fi

## -- Command Requried
if ! [[ -z "${COMMAND}" ]]; then if ! [[ "$COMMAND" == *"FILE"* ]]; then echo "No 'FILE' reference found in command!" && exit 1; fi; else echo "Command (-c) required" && exit 1; fi


## -- Extension Validation
if ! [[ -z $EXTENSIONS ]]; then
  if [[ $EXTENSIONS == *","* ]]; then
    IFS=',' read -ra EXT <<< "$EXTENSIONS"
    for extension in ${EXT[@]}; do
      STATEMENT="$STATEMENT -name '*.$extension' -type f"
      ! [[ $(echo "${EXT[-1]}" | sed 's/ //g') == *"${extension}"* ]] && STATEMENT="$STATEMENT -or"
    done
  else
    STATEMENT="-iname \"*.${EXTENSIONS}\" -type f"
  fi
fi

## -- Exclusion Validation
if ! [[ -z $EXCLUDES ]]; then
  if [[ $EXCLUDES == *","* ]]; then
    IFS=',' read -ra EXC <<< "$EXCLUDES"
  else
    EXC=("$EXCLUDES")
  fi
fi

## -- Find all Files
FIND_CMD=$(echo "find $SOURCE_DIRECTORY $STATEMENT");
for file in $(eval $FIND_CMD);
do
  ## --- Check Execlude
  if ! [ -z "${EXC}" ]; then
    BREAK="false"
    for i in "${EXC[@]}"
    do
      if [[ "${file}"  == *"${i}"* ]]; then
        BREAK="true" && continue;
      fi
    done
    [[ "${BREAK,,}" == "true" ]] && continue;
  fi
  ## -- Check if Command should be executed
  ${COMMAND/FILE/$file}
  [[ $? -ne 0 ]] && CONTAINS_ERROR="true"
done
! [[ -z "${CONTAINS_ERROR}" ]] && exit 1 || exit 0
