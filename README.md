# File Finders

Simple Script to find variable extensions, variable execlude files.

## Example

Run Yamllint in relaxed mode foreach found file matching on of the given extensions.

```
bash file-finder/find-files.sh -s "myfiles/" -e "yml,yaml,ejson,json" -c "yamllint -d relaxed -f colored FILE"
```

## Parameters

The following parameters are currently supported:

```
Usage: find-files.sh [-h] [-s] "source_dir" [-e] "ext1,ext2" [-E] "execlude1,execlude2" [-c] "mycommand "
    -s source_dir      Cache-File [Default: '.']
    -e extensions      Comma separated list of extensions [Default: 'yml,yaml']
    -E excludes        Comma separated list of excluded files/directories [Default: '.gitlab-ci.yml,node_modules,__pycache__']
    -c command         Execute command with file placeholer. Use 'FILE' as placeholder [Example: 'yamllint FILE']
    -h                 Show this context
```
